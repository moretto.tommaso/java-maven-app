def testing() {
    echo "testing the application for branch $BRANCH_NAME ..."
    sh 'mvn test'
} 

def buildJar() {
    echo "building the application..."
    sh 'mvn package'
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t tmoretto17/my-app:jma-3.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push tmoretto17/my-app:jma-3.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this